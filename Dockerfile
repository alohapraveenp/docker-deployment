FROM ubuntu:17.04
MAINTAINER Praveen Patil <praveenp@alohatechnology.com>
RUN apt-get update -y
RUN apt-get update && apt-get -y install  php7.0 php7.0-mysql git curl wget sudo unzip && apt-get clean && rm -rf /var/lib/apt/lists/*


# Install app
#RUN rm -rf /var/www/html/*
#ADD src /var/www/html

# Configure apache
RUN a2enmod rewrite
RUN chown -R www-data:www-data /var/www/html
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

COPY src/ /var/www/html/


EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D",  "FOREGROUND"] 
#/usr/sbin/apache2ctl -D FOREGROUND
