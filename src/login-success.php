<?php
session_start();
$validUser = $_SESSION["login"];

if(isset($_POST["logout"])) {
	$validUser = $_SESSION["login"] === false;
	session_destroy();
 }

if(!$validUser) {
   header("Location: /index.php"); die();
}
?>

<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Login Form</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
	<link rel="stylesheet" href="css/style.css">
</head>

<body>
<!-- Mixins-->
<div class="pen-title">
  <h1>Home Page</h1></span>
</div>
<div class="container">
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Hello Admin 	
      <div class="close"></div>
    </h1>
    <form method="post" action="" name="input">
      <div class="button-container">
        <button type="submit" name="logout" ><span>Logout</span></button>
      </div>
    </form>
  </div>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>

</body>
</html>